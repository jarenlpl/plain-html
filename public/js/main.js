// Main JavaScript Document


window.addEventListener('load', () => {

	let lat = 42.984562;
	let long = -81.246535;

	const proxy = 'https://cors-anywhere.herokuapp.com/';
	let api = `${proxy}https://api.darksky.net/forecast/c0cbd8cfc17c95cf2a14184a5228107b/${lat},${long}`;

	let tempHourlyDesc = document.querySelector('.temp-hourly-desc');
	let iconHourlyCon = document.querySelector('.icon-hourly-con');

	let tempDailyDesc = document.querySelector('.temp-daily-desc');
	let iconDailyCon = document.querySelector('.icon-daily-con');

	let tempDailyTemperatureMax = document.querySelector('.temp-daily-temperatureMax');
	let tempDailyTemperatureMin = document.querySelector('.temp-daily-temperatureMin');
	let tempCurrentTemp = document.querySelector('.temp-current-temperature');

	let tempDailyInfo1Desc = document.querySelector('.temp-daily-info1-desc');
	let iconDailyInfo1Con = document.querySelector('.icon-daily-info1-con');

	let tempDailyInfo2Desc = document.querySelector('.temp-daily-info2-desc');
	let iconDailyInfo2Con = document.querySelector('.icon-daily-info2-con');

	let tempDailyInfo3Desc = document.querySelector('.temp-daily-info3-desc');
	let iconDailyInfo3Con = document.querySelector('.icon-daily-info3-con');


	// Convert to Celcius
	function convertMetric(temp) {
		let celcuis = Math.round((temp - 32) * (5 / 9));
		return celcuis;
	}

	// Skycons
	function setIcons(icon, color, iconID) {
		const skycons = new Skycons({
			color: color
		});
		const currentIcon = icon.replace(/-/g, "_").toUpperCase();
		skycons.play();

		return skycons.set(iconID, Skycons[currentIcon]);
	}

	// Week overview
	function updateDate(daysAhead) {
		let today = new Date();
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday'];
		let todayNum = today.getDay();

		let day = days[todayNum + daysAhead];

		return day;
	}

	let weekDate = document.querySelectorAll('.customDate');

	for (i = 0; i < weekDate.length; i++) {
		weekDate[i].textContent = updateDate(i + 1);
	}


	fetch(api)
		.then(response => {
			return response.json();
		})
		.then(data => {
			//console.log(data);

			//Set DOM elements from API
			tempHourlyDesc.textContent = data.hourly.summary;
			setIcons(data.hourly.icon, "gray", iconHourlyCon);

			tempDailyDesc.textContent = data.daily.summary;
			setIcons(data.daily.icon, "white", iconDailyCon);

			tempDailyTemperatureMax.textContent = convertMetric(data.daily.data[0].temperatureMax);
			tempDailyTemperatureMin.textContent = convertMetric(data.daily.data[0].temperatureMin);
			tempCurrentTemp.textContent = convertMetric(data.currently.temperature);

			tempDailyInfo1Desc.textContent = data.daily.data[0].summary;
			setIcons(data.daily.data[0].icon, "white", iconDailyInfo1Con);

			tempDailyInfo2Desc.textContent = data.daily.data[1].summary;
			setIcons(data.daily.data[1].icon, "white", iconDailyInfo2Con);

			tempDailyInfo3Desc.textContent = data.daily.data[2].summary;
			setIcons(data.daily.data[2].icon, "white", iconDailyInfo3Con);
		})

});
